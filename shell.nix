# Great info here: https://maybevoid.com/posts/2019-01-27-getting-started-haskell-nix.html
# $ nix-shell --pure shell.nix

{ nixpkgs ? import <nixpkgs> {} }:
let
  inherit (nixpkgs) pkgs;
  inherit (pkgs) haskellPackages;

  haskellDeps = ps: with ps; [
    base
    hakyll
    random
  ];

  ghc = haskellPackages.ghcWithPackages haskellDeps;

  nixPackages = [
    ghc
    pkgs.gdb
    haskellPackages.cabal-install
    # haskellPackages.stack
    # haskellPackages.summoner
    haskellPackages.hakyll
    pkgs.zlib
    pkgs.git
    pkgs.curl
    pkgs.binutils-unwrapped
    pkgs.pkgconfig
    pkgs.which
    haskellPackages.cabal2nix
  ];
in
pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs = nixPackages;
}
